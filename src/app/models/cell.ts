export interface Cell {
  x: number;
  y: number;
  isVisited: boolean;
  north: boolean;
  south: boolean;
  east: boolean;
  west: boolean;
}
