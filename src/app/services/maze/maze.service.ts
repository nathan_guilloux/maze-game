import { Cell } from './../../models/cell';
import { Injectable } from '@angular/core';
import { Player } from 'src/app/models/player';
import { KEY_CODE } from 'src/app/models/keycode';

@Injectable({
  providedIn: 'root'
})
export class MazeService {

  maze!: Cell[][];
  activePosition!: Cell;
  player!: Player;

  constructor() {
    this.startGame();
  }

  startGame(): void {
    this.maze = this.initMaze(20);
    this.activePosition = this.maze[0][0];
    this.generate(0, 0);
    this.player = { x: 0, y: 0 };
  }

  move(direction: KeyboardEvent): boolean | undefined {
    if(direction.code === KEY_CODE.DOWN_ARROW || direction.code === KEY_CODE.UP_ARROW || direction.code === KEY_CODE.LEFT_ARROW || direction.code === KEY_CODE.RIGHT_ARROW) {
      direction.preventDefault();
    }
    if (direction.code === KEY_CODE.DOWN_ARROW) {
      if (this.player.y + 1 < this.maze.length && this.maze[this.player.y][this.player.x].south) {
        this.player.y += 1;
        return true;
      } else {
        return false;
      }
    } else if (direction.code === KEY_CODE.UP_ARROW) {
      if (this.player.y - 1 >= 0 && this.maze[this.player.y][this.player.x].north) {
        this.player.y -= 1;
        return true;
      } else {
        return false;
      }
    } else if (direction.code === KEY_CODE.LEFT_ARROW) {
      if (this.player.x - 1 >= 0 && this.maze[this.player.y][this.player.x].west) {
        this.player.x -= 1;
        return true;
      } else {
        return false;
      }
    } else if (direction.code === KEY_CODE.RIGHT_ARROW) {
      if (this.player.x + 1 < this.maze.length && this.maze[this.player.y][this.player.x].east) {
        this.player.x += 1;
        return true;
      } else {
        return false;
      }
    } else {
      return undefined;
    }
  }

  initMaze(size: number): Cell[][] {
    let mazeTemporary: Cell[][] = [];
    for (let i = 0; i < size; i++) {
      mazeTemporary.push([]);
      for (let j = 0; j < size; j++) {
        mazeTemporary[i].push({
          x: j,
          y: i,
          isVisited: false,
          north: false,
          south: false,
          east: false,
          west: false
        });
      }
    }
    return mazeTemporary;
  }

  generate(x: number, y: number) {
    this.maze[y][x].isVisited = true;
    let neighbours = this.getNeighbour(this.maze[y][x]);
    let hasNeighbours = !neighbours[0].isVisited || !neighbours[1].isVisited || !neighbours[2].isVisited || !neighbours[3].isVisited;
    while (hasNeighbours) {
      let randomNumber = this.randomInteger(0, 3);
      if (!neighbours[randomNumber].isVisited) {
        let randomNeighbour = neighbours[randomNumber];
        if (randomNumber === 0) { // North neighbour
          this.maze[randomNeighbour.y][randomNeighbour.x].south = true;
          this.maze[y][x].north = true;
        } else if (randomNumber === 1) { // South neighbour
          this.maze[randomNeighbour.y][randomNeighbour.x].north = true;
          this.maze[y][x].south = true;
        } else if (randomNumber === 2) { // West neighbour
          this.maze[randomNeighbour.y][randomNeighbour.x].east = true;
          this.maze[y][x].west = true;
        } else if (randomNumber === 3) { // East neighbour
          this.maze[randomNeighbour.y][randomNeighbour.x].west = true;
          this.maze[y][x].east = true;
        }
        this.generate(randomNeighbour.x, randomNeighbour.y);
        hasNeighbours = !neighbours[0].isVisited || !neighbours[1].isVisited || !neighbours[2].isVisited || !neighbours[3].isVisited;
      }
    }
  }

  getMaze(): Cell[][] {
    return this.maze;
  }

  getPlayer(): Player {
    return this.player;
  }

  getNeighbour(cell: Cell): Cell[] {
    let result: Cell[] = [];
    result.push(cell.y - 1 >= 0 ? this.maze[cell.y - 1][cell.x] : { // North neighbour
      x: -10,
      y: -10,
      isVisited: true,
      north: false,
      south: false,
      east: false,
      west: false
    });
    result.push(cell.y + 1 < this.maze.length ? this.maze[cell.y + 1][cell.x] : { // South neighbour
      x: -10,
      y: -10,
      isVisited: true,
      north: false,
      south: false,
      east: false,
      west: false
    });
    result.push(cell.x - 1 >= 0 ? this.maze[cell.y][cell.x - 1] : { // West neighbour
      x: -10,
      y: -10,
      isVisited: true,
      north: false,
      south: false,
      east: false,
      west: false
    });
    result.push(cell.x + 1 < this.maze.length ? this.maze[cell.y][cell.x + 1] : { // East neighbour
      x: -10,
      y: -10,
      isVisited: true,
      north: false,
      south: false,
      east: false,
      west: false
    });
    return result;
  }

  randomInteger(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
