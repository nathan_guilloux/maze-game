import { Cell } from './../models/cell';
import { MazeService } from './../services/maze/maze.service';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Player } from '../models/player';
import { KEY_CODE } from '../models/keycode';

@Component({
  selector: 'app-maze',
  templateUrl: './maze.component.html',
  styleUrls: ['./maze.component.css']
})
export class MazeComponent implements OnInit {

  @ViewChild('mazeElem') mazeElement!: ElementRef<any>;
  maze: Cell[][];
  goal: any;
  player!: Player;
  shake: boolean;
  goalReached: boolean;
  flipped: boolean;

  constructor(private mazeService: MazeService) {
    this.maze = [];
    this.shake = false;
    this.goalReached = false;
    this.flipped = false;
  }

  ngOnInit(): void {
    this.mazeService.startGame();
    this.maze = this.mazeService.getMaze();
    this.player = this.mazeService.getPlayer();
    this.goal = { x: this.maze.length - 1, y: this.maze.length - 1 };
  }

  shakeIt(): void {
    this.shake = true;
    setTimeout((arg: any) => {
      this.shake = false;
    },
      300);
  }

  solution(): void {
    console.log("Not so fast !");
  }

  flip(): void {
    this.flipped = !this.flipped;
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (!this.goalReached) {
      if (this.mazeService.move(event) === false) {
        console.log("nope");
        this.shakeIt();
      } else {
        if (this.player.x === this.goal.x && this.player.y === this.goal.y) {
          this.goalReached = true;
        }
      }
    }
  }
}
